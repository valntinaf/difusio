# Difusio

Virus spread simulation in dash. SI and SIR model.

Some python modules are required for the correct execution of the models.
* dash
* networkx
* pandas
* plotly

When launching either of the models, Das will use the 8050 port and you'll be able to test the app at http://localhost:8050.

> If you cannot visualize the app, check your firewall settings
